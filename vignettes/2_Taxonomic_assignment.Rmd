---
title: "Taxonomic assignment"
output: html_notebook
---

Load the ASV table if it is not still loaded in your environment.
```{r}
library(Microbial)
library(DECIPHER)
load(file = '~/Desktop//seqtab_nochim_lize.RData')
```

# DECIPHER IDTAXA
Download the pre-trained classifier here: http://www2.decipher.codes/Classification/TrainingSets/SILVA_SSU_r138_2019.RData
```{r}
dna <- DNAStringSet(getSequences(seqtab.nochim)) 
load("~/Desktop/SILVA_SSU_r138_2019.RData") # CHANGE TO THE PATH OF YOUR TRAINING SET
ids <- IdTaxa(dna, trainingSet, strand="both", processors=10, verbose=TRUE) 
ranks <- c("domain", "phylum", "class", "order", "family", "genus", "species") 


# Convert the output object of class "Taxa" to a matrix analogous to the output from assignTaxonomy
taxa <- t(sapply(ids, function(x) {
        m <- match(ranks, x$rank)
        taxa <- x$taxon[m]
        taxa[startsWith(taxa, "unclassified_")] <- NA
        taxa
}))
colnames(taxa) <- ranks; rownames(taxa) <- getSequences(seqtab.nochim)

```

Now let's look at the top rows in the table 
```{r}
taxa.print <- taxa
rownames(taxa.print) <- NULL
head(taxa.print)
```

# Store the taxa in a file
```{r}
save(taxa, file = '~/Desktop//taxa.RData')
```
